*****PHONEGAP PLUGINS******

** För video capture ***

cordova plugin add org.apache.cordova.media-capture

**För att kunna använda alert dialoger:

phonegap plugin add cordova-plugin-dialogs


**För hantering av kamera:

phonegap plugin add cordova-plugin-camera


**För hantering av Device. Se hårdvarans info, uuid, modell m.m.:

phonegap plugin add cordova-plugin-device

**För att kunna använda parse.com och pushnotifikationer

cordova plugin add https://github.com/benjie/phonegap-parse-plugin --variable APP_ID=PARSE_APP_ID --variable CLIENT_KEY=PARSE_CLIENT_KEY

PARSE_APP_ID och PARSE_CLIENT_KEY  finns att få under parse.com / settings / keys.

länk: https://github.com/avivais/phonegap-parse-plugin


**
Facebook plugin. Du måste ha med variable APP_ID med det id som vi får ifrån facebook samt
variable APP_NAME med namnet som vi har på facebook. 

cordova plugin add com.phonegap.plugins.facebookconnect.kl --variable APP_ID=436730876498811 --variable APP_NAME=laQuestioneTest