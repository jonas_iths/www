// Old file send working

app.controller('answerCtrl' ,["$scope", "$location", "userService", function($scope, $location, us){
    console.log("answerCtrl");
    
    var back = false;
    var videoSrc;
    var user;

    $scope.checked = false; // This will be binded using the ps-open attribute
      $scope.toggle = function(){
      $scope.checked = !$scope.checked
    }

    // Sert user-var and check if user is banned.
    us.getUser(function (result) {
        user = result;
        if (result[0] && result[0].get("blocked")) {
            navigator.notification.beep(1);
            $scope.$apply(function () {
                $location.path("/banned");
            });
        }
    });

    var users = Parse.Object.extend("users");

    // BackButton handler
    document.addEventListener("backbutton", onBackKeyDown, false);
    function onBackKeyDown() {
        $scope.$apply(function () {
            if(back){
                $scope.closePhoto();
            } else {
                $location.path("/startpage");
        document.removeEventListener("backbutton", onBackKeyDown);
            }
            
        });
    }
    /*
     * Capture videoanswere
     *
     */
    $scope.captureVideo = function(){
        var options = { duration: 10, limit: 1 };
        console.log("capture video pressed");
        navigator.device.capture.captureVideo(captureSuccess, captureError, options);
    }
    var captureSuccess = function(mediaFiles){
        videoSrc = mediaFiles[0];
        console.log("video success!");
        var ap = document.getElementById("vidA");
        ap.setAttribute("src", mediaFiles[0].fullPath);
        ap.setAttribute("type", mediaFiles[0].type);
        document.getElementById("vidA").play();
        console.log(mediaFiles);
        // set source to fullscreen video-tag
        var vid = document.getElementById("fullscreenVid");
        vid.setAttribute("src", videoSrc.fullPath);
        vid.setAttribute("type", videoSrc.type);
    };


    var captureError = function(error){
        console.log("Cam Capture Error:"); console.log(error);
    };

    //Play QuestionVideo in fullscreen video-tag.
    $scope.playVideo = function(){
        var vid = document.getElementById("fullscreenVid");
        vid.style.display = 'inline';
        vid.play();
        vid.onended = function(){
            vid.style.display = 'none';
            console.log("Video ended");
        }

    }

    //play answerevideo in vidA-div.
    $scope.playVideoA = function(){
        document.getElementById("vidA").play(); // kommentera ur denna rad och lägg in det nedre för att visa i fullscreenDiv.
    }

	$scope.question = "";
    var question = Parse.Object.extend("questions");
    var query = new Parse.Query(question);
    var deviceIdOfQuestion;
    var qResult;

	// check if currently answering question
	var check = Parse.Object.extend("questions");
	var checkQuery = new Parse.Query(check);
	checkQuery.equalTo("currentAnswerPerson", window.device.uuid);
	checkQuery.find({	
  	success: function(results) {    	
				
      	var object = results[0];
      	
		if (object) {	
			// load old question
			qResult = object;
			
			$scope.$apply(function() {
				$scope.video = object.get("video");
			});

		}
        else {
			// check for new question			
			
			query.equalTo("status", 0);			
			//query.notEqualTo("personID", window.device.uuid);   //<<<-----  Insert this code to NOT being able to answer your own Q's
			query.ascending("createdAt");
			query.first().then(function(result) {
			  
			  $scope.$apply(function() {
				if(result){
				  qResult = result;  //Behöver spara resultatet för att komma åt .id när vi sänder tebax svar.
				  result.set("status", 1);
				  result.set("currentAnswerPerson", window.device.uuid);
				  result.save();
				  $scope.video = result.get("video");

				}
                else{
                        console.log("Error. Det skall finnas en fråga men ingen kom?");
				}
			  });
			});
		}
	
	},
  	error: function(error) {
    	alert("Error: " + error.code + " " + error.message);
  	}
	});		    
        
    

    $scope.reportQ = function(){
      navigator.notification.prompt(
        'Reason for report:',
        onPrompt,
        'Report',   
        ['Ok','Exit']
      );

      function onPrompt(results, buttonIndex) {
        if(buttonIndex === 0){
          console.log("Exit report");
        }else{
          if(results.input1){
            qResult.set("answer", "WARNING! Reported with reason: " + results.input1);
            qResult.set("status", 2);
            qResult.save(null, {
              success: function(point) {
                  console.log("Save?");
                  // Saved successfully.
              },
              error: function(point, error) {
                console.log("Failed to save. Syntax error. Not DB error.");
              }
            });
            
            var user = Parse.Object.extend("users");
            var query = new Parse.Query(user);

            query.equalTo("UserID", qResult.get("personID"));
            query.first().then(function(result) {
              result.add("report", qResult);
              result.save(null, {
                success: function(point) {
                  $scope.$apply(function() {
                    $location.path("/startpage");
                  });
                },
                error: function(point, error) {
                  console.log("Failed to save. Syntax error. Not DB error.");
                }
              });

            });
          }else {
            qResult.set("status", 0);
            qResult.save(null, {
              success: function(point) {
                  $scope.$apply(function() {
                    $location.path("/startpage");
                  });
              },
              error: function(point, error) {
                  console.log("Failed to save. Syntax error. Not DB error.");
              }
            });
          }
        }
      }  
    }


    $scope.answer = function(){        
	  
	  var user = Parse.Object.extend("users");
      var query = new Parse.Query(user);
      var fbData;
      
      query.equalTo("UserID",window.device.uuid);
      query.first().then(function(result) {
          
              console.log(result);
              fbData = result.attributes.faceBookData;
              console.log("fbData i answer");
              console.log(fbData);
              
              var curYear = new Date().getFullYear()
              var tempAge = fbData.birthday.slice(-4);
              age = curYear - tempAge;
              console.log("senders age is: " + age);
              qResult.set("answer", $scope.thetext);
              qResult.set("status", 2);
			  qResult.set("currentAnswerPerson", "");
              qResult.set("senderData", {age: age, gender: fbData.gender, uuid: window.device.uuid});
              qResult.save(null, {
                  success: function(point) {
                      console.log("Kommer jag till success?");
                      $scope.$apply(function() {
                        $location.path("/startpage");
                      });
                      // Saved successfully.
                  },
                  error: function(point, error) {
                      // The save failed.
                      // error is a Parse.Error with an error code and description.
                      console.log("Failed to save. Syntax error. Not DB error.");
                  }
              });
      });

        console.log("answer");
        var userIdOfQuestion = qResult.get("personID");
        // Ändrar på kolumnen answere och sparar.
        var queryId = new Parse.Query(users);

        queryId.equalTo("UserID", userIdOfQuestion);
        queryId.find().then(function(results) {
            console.log(results);
            console.log(userIdOfQuestion);
            deviceIdOfQuestion = "" + results[0].get("installId");
            console.log(deviceIdOfQuestion);
                Parse.Cloud.run('noticeUser', {installid: deviceIdOfQuestion}, {
                    success: function (result) {
                        console.log("noticeUser was called");
                        //Doing alright!
                    },
                    error: function (error) {
                        console.log("Couldn't do it.. Syntax error");
                        console.log(error);
                    }
                });

        });
    }
     
    	
}]);