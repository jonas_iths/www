/**
 * Created by the Fab. Five on 26/05/15.
 */

app.controller('askFormCtrl',['$scope','$location', 'userService', function($scope, $location, us){

    us.getUser(function (result) {
        user = result;
        if (result[0] && result[0].get("blocked")) {
            navigator.notification.beep(1);
            $scope.$apply(function () {
                $location.path("/banned");
            });
        }
    });

	var back = false;
	
	$scope.closePhoto = function(){
    	var smallImage = document.getElementById('smallImageDiv');
      	smallImage.style.display = 'none';
        back = false;
    }

    document.addEventListener("backbutton", onBackKeyDown, false);
    function onBackKeyDown() {
        $scope.$apply(function () {
            if(back){
                console.log("display inline");
                $scope.closePhoto();				
				// $location.path("/askQuestion");
            } else {
                console.log("display else");
                $location.path("/startpage");
				document.removeEventListener("backbutton", onBackKeyDown);
            }
            
        });
    }
    
    var pictureSource;   // picture source
    var destinationType; // sets the format of returned value
    var imgSrc;
    $scope.takePhoto = function(){
    	pictureSource = navigator.camera.PictureSourceType;
        destinationType = navigator.camera.DestinationType;
    	
        console.log("Ta en bild");
    	navigator.camera.getPicture(onPhotoDataSuccess, onFail, { 
            quality: 50,
    	    destinationType: destinationType.DATA_URL,
            targetWidth: 500,
            targetHeight: 500,
            correctOrientation: true
        });
    }

    // video capture
    $scope.captureVideo = function(){
        var options = { duration: 10,
                        limit: 1
         };
        console.log("capture video pressed");
        navigator.device.capture.captureVideo(captureSuccess, captureError, options);
    }
    var captureSuccess = function(mediaFiles){
        console.log("video success!");
        var ap = document.getElementById("video");
        ap.setAttribute("src", mediaFiles[0].fullPath);
        ap.setAttribute("type", mediaFiles[0].type);
        document.getElementById("video").play();
        imgSrc = mediaFiles;
        console.log(mediaFiles);
    };

    var uploadFile = function(mediaFiles){
        var ft = new FileTransfer(),
            path = mediaFile.fullPath,
            name = mediaFile.name;
    };
    var captureError = function(error){
        console.log(error);
    };

	$scope.playVideo = function(){
      document.getElementById("video").play();
    }
    //handling fotocaption -  disabled
	$scope.showPhoto = function(){
    	var smallImage = document.getElementById('smallImageDiv');
      	smallImage.style.display = 'inline';
        back = true;
    }		
	
    function onPhotoDataSuccess(imageData) {
      	imgSrc = imageData;
		var smallImage = document.getElementById('smallImage');

      	smallImage.style.display = 'block';
		
		var showPhotoButton = document.getElementById('showPhotoButton');
		showPhotoButton.style.display = 'block';

      	smallImage.src = "data:image/jpeg;base64," + imageData;				
		
    }

    function onFail(message) {
  		alert('Failed because: ' + message);
	}

    $scope.submitForm = function(event){

        //console.log('Question:' + $scope.form.question);
        
        var question = Parse.Object.extend("questions");
        var Q = new question();        
        
        if (imgSrc) {
            console.log("IF");
            var base64 = "data:image/jpeg;base64," + imgSrc
            var file = new Parse.File("qPic.jpeg", { base64: base64 });
            Q.save({title: $scope.form.title, question: $scope.form.question, name: $scope.form.name, image: file, status: 0, personID: window.device.uuid});
        }else {
            console.log("else");
            Q.save({title: $scope.form.title, question: $scope.form.question, name: $scope.form.name, status: 0, personID: window.device.uuid});
        }
        
        $location.path("/startpage");
    };

}]);