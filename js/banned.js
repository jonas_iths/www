
app.controller('bannedCtrl', ['$scope', function($scope){

    document.addEventListener("backbutton", onBackKeyDown, false);
    function onBackKeyDown(e) {
        console.log("exit app");
        e.preventDefault();
        navigator.app.exitApp();
    }

    $scope.exit = function (){
        console.log("exit app");
        navigator.app.exitApp();
    };

}]);
