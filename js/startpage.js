app.controller('startpageCtrl', ['$scope', '$location', 'userService', function($scope, $location, us){
    console.log("startpageCtrl");

    us.getUser(function (result) {
        if (result[0] && result[0].get("blocked")) {
            navigator.notification.beep(1);
            $scope.$apply(function () {
                $location.path("/banned");
            });
        }
    });

    $scope.checked = false; // This will be binded using the ps-open attribute
                $scope.toggle = function(){
                    $scope.checked = !$scope.checked
                    console.log($scope.checked);

    }

    var user = Parse.Object.extend("users");
    var query = new Parse.Query(user);
    query.equalTo("UserID",window.device.uuid);
    query.first().then(function(result) {
        $scope.$apply(function() {
            console.log(result);
            $scope.fbData = result.attributes.faceBookData;
            $scope.thePic = "http://graph.facebook.com/" + result.attributes.faceBookData.id + "/picture?type=normal";
        });
});

    /*
     * Add listener to backbutton. Prevent buttons default behavior and give user Yes/No option for quit.
     */
    document.addEventListener("backbutton", onBackKeyDown, false);
    function onBackKeyDown(e) {
        e.preventDefault();
        navigator.notification.confirm("Are you sure you want to exit ?", onConfirm, "Confirmation", "Yes,No");
    }

    function onConfirm(button) {
        if (button == 2) {
            return;
        } else {
            navigator.app.exitApp();
        }
    }


    $scope.newQ = function(){
        if($scope.checked == true){
            $scope.toggle();
        }
        else if($scope.checked == false) {
            var question = Parse.Object.extend("questions");
            var query = new Parse.Query(question);

            query.equalTo("personID", window.device.uuid);
            query.find({
                success: function(result) {
                    console.log(result);
                    console.log(result.length);
                    $scope.$apply(function() {

                        if(result.length === 10){

                            alert("You've to many Qs in the air");
                        }else {
                        
                            $location.path("/askQuestion");
                            document.removeEventListener("backbutton", onBackKeyDown);
                        }
                    });    
                },
                error: function(error) {
                    alert(error);
                }
            });   
        }
        
    }

    $scope.answerQ = function(){
        if($scope.checked == true){
            $scope.toggle();			
        }
        else if($scope.checked == false) {											
								
				// check if question exists to answer				
				var check = Parse.Object.extend("questions");
				var checkQuery = new Parse.Query(check);				
				
				checkQuery.equalTo("currentAnswerPerson", window.device.uuid);
				checkQuery.find({	
				success: function(results) {    	
							
					var object = results[0];
					
					// Check for currently answering
					if (object) {						
						$scope.$apply(function () {								
								$location.path("/answer");
								document.removeEventListener("backbutton", onBackKeyDown);
						});							        																									
					} else {
						// check for new question	
						var question = Parse.Object.extend("questions");
						var query = new Parse.Query(question);														
						query.equalTo("status", 0);
						query.notEqualTo("personID", window.device.uuid);						
						query.first().then(function(result) {
						  						  
						if(result){								
							$scope.$apply(function () {								
								$location.path("/answer");
								document.removeEventListener("backbutton", onBackKeyDown);
							});																			  
						} else {
							navigator.notification.alert(
								'There is no question to answer. Try again later!', // message
								doNothing,         									// callback
								'Oooops',	            							// title
								'Ok'                  								// buttonName
							);
										  							 
						}						  
						});
					}
				
				},
				error: function(error) {
					alert("Error: " + error.code + " " + error.message);
				}
				});
				
        }		

	}
		
	function doNothing() {
    	// do something
	}		 

    $scope.myQs = function(){
        if($scope.checked == true){
            $scope.toggle();
        }
        else if($scope.checked == false) {
                $location.path("/myqs");
        }
        document.removeEventListener("backbutton", onBackKeyDown);
    }
    
    var boxChecked;
    var user;
    var db = Parse.Object.extend("users");
    var query = new Parse.Query(db);
    query.equalTo("UserID", window.device.uuid);
    query.find({
        success: function(result){
            user = result[0]
            $scope.checkboxModule = {
                push : user.get("allowPush"),
                name : false
            };
        }, error: function(error){
            status.error("Error in user queary: " + error);
        }
    });



    $scope.saveData = function(){
        user.save({allowPush : $scope.checkboxModule.push, showName : $scope.checkboxModule.name});
        console.log("saved new allowPush status");
        $scope.push = $scope.checkboxModule.push;
    }

}]);