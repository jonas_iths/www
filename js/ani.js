app.controller('animationCtrl', ['$scope', '$location', '$timeout', 'userService', function($scope, $location, $timeout, us){
    console.log("animationCtrl");

    document.addEventListener('deviceready', function() {
        installId = "";
        console.log("deviceready");
        console.log(window.device);

        //Init parse Installation (Device installation)
        parsePlugin.initialize(
            "7iBeKQfOokKRZttfaPOeOqU42XA8hj3D5KltSHyB",
            "M8bNuoG8V6sh2BH6H50zuArHyG37519lwk0nICBg",
            function() {
                console.log( 'PARSE INIT OK' );
                parsePlugin.getInstallationId(function(id) {
                    installId = id;
                }, function(e) {
                    console.log("Error Getting ID: " + e.code + " : " + e.message);
                })
            },
            function( e ) {
                console.log( 'PARSE FAILED' );
            }
        );

        

        

        //Check for userId and duplicates
        var userID = Parse.Object.extend("users");
        var query = new Parse.Query(userID);
        
        console.log(window.device.uuid);
        query.equalTo("UserID", window.device.uuid);
        query.find({
            success: function(result) {
                console.log(result);
                console.log(result.length);
                var fbData;
                
                var fbLoginSuccess = function (userData) {
                    var uID = new userID();
                    
                    if(result.length === 0){
                        navigator.notification.confirm(
                        "We use facebook login, click OK to go forth.",
                        onPrompt,
                        'Login',   
                        ['Ok','Exit']);

                        function onPrompt(buttonIndex) {
                           // alert("You have pressed " + buttonIndex);
                            if(buttonIndex === 0){
                                navigator.app.exitApp();
                            }else {
                                facebookConnectPlugin.api( "me/?fields=id,email,name,gender,birthday,location,locale,timezone,link", ["user_birthday"],
                                    function (response) { 
                                        console.log("success");
                                        console.log(response); 
                                        fbData = response;
                                        console.log(fbData); 
                                        uID.save({
                                            UserID: window.device.uuid,
                                            Platform: window.device.platform,
                                            installId: installId,
                                            faceBookData: fbData,
                                            allowPush: true,
                                            showName: false
                                        });
                                    },
                                    function (errorMsg) { 
                                        alert("Failed to connect to facebook");
                                        console.log(errorMsg);
                                        navigator.app.exitApp();
                                });
                                $scope.$apply(function() {
                                    if (userData.status == 'connected') {
                                        $location.path("/startpage");
                                    }else {
                                        alert("Failed to connect to facebook");
                                        navigator.app.exitApp();
                                    }
                                });
                            }    
                        }
                    }else {
                        us.userDb = result[0];
                            console.log("UserID already exists");
                        //Check if user is banned.
                        if(result[0].get("blocked")){
                            navigator.notification.beep(1);
                            $scope.$apply(function(){
                                $location.path("/banned");
                            });
                        } else {
                            $scope.$apply(function () {
                                if (userData.status == 'connected') {
                                    $location.path("/startpage");
                                } else {
                                    alert("Failed to connect to facebook");
                                    navigator.app.exitApp();
                                }
                            });
                        }
                    }
                }
                facebookConnectPlugin.login(["public_profile"],
                    fbLoginSuccess, 
                    function (error) { alert("" + error) 
                });

               
            },
            error: function(error) {
                console.log("Not good enough");
                console.log(error);
            }

        });   
        
 
        
    }, true);
    
    //rotateAnimation("laQlogo", 1);
    // $timeout(function(){ 
    // },4000);

    
}]);

var degrees = 0;
var looper;
function rotateAnimation(el, speed){
    var elem = document.getElementById(el);
    if(navigator.userAgent.match("Chrome")){
        elem.style.WebkitTransform = "rotate("+degrees+"deg)";
    } else if(navigator.userAgent.match("Firefox")){
        elem.style.MozTransform = "rotate("+degrees+"deg)";
    } else {
        elem.style.transform = "rotate("+degrees+"deg)";
    }
    looper = setTimeout('rotateAnimation(\''+el+'\','+speed+')',speed);
    degrees++;
    elem.style.fontSize = degrees * 7 + "%";
    
    if(degrees > 360){
        degrees = 0;
        clearTimeout(looper);
    }
}