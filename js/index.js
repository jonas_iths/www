var app = angular.module('laQuestione', ['ngRoute', 'ngTouch','pageslide-directive']);

    var switchBool = true;
    app.filter('reverser', function(){
        return function(zeArray){

            if(!angular.isArray(zeArray)){
                if(typeof zeArray === 'object'){
                    var tempArray = [];
                    for(var i in zeArray){
                        zeArray[i].id = i;
                       tempArray.push(zeArray[i]);
                    }
                   return tempArray.slice().reverse();
               }
                return zeArray;
            }
            return zeArray.slice().reverse();
        };
    });

    app.filter('colors', function(){
        return function(questions){
            console.log("inne i colors filter");
            if(switchBool){
                console.log("IF");
                document.getElementById("myInnerQs").style.backgroundColor = "#9999ff";
                switchBool = false;
            }else{
                console.log("else");
               document.getElementById("myInnerQs").style.backgroundColor = "#06F";
               switchBool = true;
           }
           return questions;
       };
    });

app.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/", {
        redirectTo: "startpage.html"
    });

    $routeProvider.when("/startpage", {
        templateUrl: "startpage.html"
    });
    
    $routeProvider.when("/answer", {
        templateUrl: "answer.html"
    });

    $routeProvider.when("/askQuestion", {
        templateUrl: "askquestion.html"
    });   

    $routeProvider.when("/login", {
        templateUrl: "login.html"
    });

    $routeProvider.when("/animation", {
        templateUrl: "animation.html"
    });
	
	$routeProvider.when("/myqs", {
        templateUrl: "myqs.html"
    });

    $routeProvider.when("/banned", {
        templateUrl: "banned.html"
    });

    $routeProvider.when("/contacts", {
        templateUrl: "contacts.html"
    });

    $routeProvider.when("/jonas", {
        templateUrl: "jonas.html"
    });

    $routeProvider.when("/viktor", {
        templateUrl: "viktor.html"
    });

    $routeProvider.when("/daniel", {
        templateUrl: "daniel.html"
    });

    $routeProvider.when("/mattias", {
        templateUrl: "mattias.html"
    });

    $routeProvider.when("/janne", {
        templateUrl: "janne.html"
    });
	        
}]);

app.service('userService', function(){
    var userDb;
    var getUser = function(successCallback){
        var userID = Parse.Object.extend("users");
        var query = new Parse.Query(userID);
        query.equalTo("UserID", window.device.uuid);
        query.find({
            success: successCallback,
            error: function(error){console.log(error);}
        });
    };

    return {
        "userDb": userDb,
        "getUser": getUser
    };
});
app.controller('contentCtrl', ['$scope', '$location', function($scope, $location){
    console.log("contentCtrl in index");
    Parse.initialize("7iBeKQfOokKRZttfaPOeOqU42XA8hj3D5KltSHyB", "DJtAzJfs8zpTE8idqcCOBhCbpJEUxsJPxd6G2YWT");
    $location.path("/animation");
}]);


