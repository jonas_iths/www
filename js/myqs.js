app.controller('myqsCtrl', ['$scope', '$location', function($scope, $location){

	var back = false;

	document.addEventListener("backbutton", onBackKeyDown, false);
	function onBackKeyDown() {
		$scope.$apply(function () {
            if(back){
                $scope.closePhoto();				
			} else {
                $location.path("/startpage");
				document.removeEventListener("backbutton", onBackKeyDown);
            }
            
        });
	}

	$scope.myQReport = function(theQ) {
		console.log("myQReport");
		var user = Parse.Object.extend("users");
      	var query = new Parse.Query(user);
      	console.log(theQ.sender.uuid);
      	
      	query.equalTo("UserID", theQ.sender.uuid);
      	
      	query.first().then(function(result) {
       		
       		result.add("report", {question: theQ.question, answer: "Reason why Reported: " + theQ.answer});
            result.save(null, {
              success: function(point) {
                $scope.$apply(function() {
			    	$scope.removeQ(theQ);
			    });
                
              },
              error: function(point, error) {
                console.log("Failed to save. Syntax error. Not DB error.");
              }
        	});
        });
	}

	$scope.showPhoto = function(img){
      var smallImage = document.getElementById('smallImageDiv');
        smallImage.style.display = 'inline';
        console.log(img);
        document.getElementById('smallImage').src = img;
        back = true;
    }
  
    $scope.closePhoto = function(){
      var smallImage = document.getElementById('smallImageDiv');
        smallImage.style.display = 'none';
        back = false;
    }

	var unAnsweredArray = [];
	var answeredArray = JSON.parse(window.localStorage.getItem("storageArray"));
	
	var myQs = Parse.Object.extend("questions");
	var qQuery = new Parse.Query(myQs);

	qQuery.equalTo("personID", window.device.uuid);
	qQuery.find({
		success: function(result) {
			if(!answeredArray){
				answeredArray = [];
			}

			for (i in result){
				if(result[i].get('status') === 2){
					console.log("answeredArray");
					
					if(result[i].get('image')){
						answeredArray.unshift({question: result[i].get('question'), answer: result[i].get('answer'), image: result[i].get('image'), sender: result[i].get('senderData')});
						console.log(answeredArray);
					} else {
						answeredArray.unshift({question: result[i].get('question'), answer: result[i].get('answer'), sender: result[i].get('senderData')});
					}

					for(i in unAnsweredArray){
						if(result[i].get('question') === unAnsweredArray[i].question){
							unAnsweredArray.splice(i,1);
						}
					}

					if (answeredArray.length > 12) {
						answeredArray.pop();														
					}

					result[i].destroy({
  						success: function(obj) {
    						// The object was deleted from the Parse Cloud.
  						},
 						error: function(error) {
    						alert("delete form parse error: " + error);
    					// error is a Parse.Error with an error code and message.
  						}
					});
				}else {
					unAnsweredArray.unshift({question: result[i].get('question')})
				}
			};

			window.localStorage.setItem("storageArray", JSON.stringify(answeredArray) );
			
			$scope.$apply(function () {
            	$scope.questions = JSON.parse(window.localStorage.getItem("storageArray"));
            	console.log(unAnsweredArray);
            	console.log("scope.questions");
            	console.log($scope.questions);
            	
            	/*if($scope.questions.image._url){
            		document.getElementById('myQImg').style.display = "inline";
            	}*/

            	$scope.unAnsweredQs = unAnsweredArray;
            });
		},error: function(error) {
    		alert("Error: " + error.code + " " + error.message);
  		}
  	});

	$scope.removeQ = function(q) {
		console.log("Removing Question");
		for(i in answeredArray){
			if(q.question === answeredArray[i].question && q.answer === answeredArray[i].answer){
				answeredArray.splice(i,1)
			}
		}
		window.localStorage.setItem("storageArray", JSON.stringify(answeredArray) );
		$scope.questions = answeredArray;
	}

 /*var GGG = Parse.Object.extend("questions");
 var query = new Parse.Query(GGG);
 query.equalTo("personID", window.device.uuid);
 query.find({
  success: function(object) {
    	
		var lengthForBackgroundColor = 0;
				
		// loop trough object
		for (var ii=0; ii<object.length; ii++) {
									
				if (object[ii].get('status') === 0 || object[ii].get('status') === 1) {
					
					if (typeof object[ii].get('image') === 'undefined') {
						imageURL = "";	
					} else {						
						var imageFile = object[ii].get('image');
  						var imageURL = imageFile.url();
					}					
						
					if ( ii % 2 ) {	
						document.getElementById("items").innerHTML += "<div style='padding:1em; width:100%; background-color:#9999ff;'><h1>Question:</h1><img src='" + imageURL + "' style='width:100%;' /><br /><h1>Answer:</h1>" + "Your question hasn't been answered yet!" + "</div>";
						
					} else {
						document.getElementById("items").innerHTML += "<div style='padding:1em; width:100%;'><h1>Question:</h1><img src='" + imageURL + "' style='width:100%;' />" + object[ii].get('question') + "<br /><h1>Answer:</h1>" + "Your question hasn't been answered yet!" + "</div>";			
					}
					
					lengthForBackgroundColor++;
					
				} else {
					
					// status == 2
					// add to local storage, check first if localstorage exists
					if(window.localStorage.storageArray) {
						// local storage exists, add to it											
																		
						var storageArray = JSON.parse(window.localStorage["storageArray"]);												
					  	storageArray.unshift({question:object[ii].get('question'), answer:object[ii].get('answer'), newAnswer: true});
					  	window.localStorage.clear();
						
						// check length of storagearray and delete if necessery
						if (storageArray.length > 12) {
							storageArray.pop();														
						}
						
					  	window.localStorage.setItem("storageArray", JSON.stringify(storageArray));																								
																																										
					} else {
						// create local storage
																		
						var array = [{question:object[ii].get('question'), answer:object[ii].get('answer')}];
					  	window.localStorage.setItem("storageArray", JSON.stringify(array) );
					}
					
					var obj = object[ii];
					obj.destroy({
  						success: function(obj) {
    						// The object was deleted from the Parse Cloud.
  					},
 						error: function(obj, error) {
    						// The delete failed.
    					// error is a Parse.Error with an error code and message.
  						}
					});
											
				}
					
		}
				 		
		// loop through local storage if it exists
				
		var storageArray = JSON.parse(window.localStorage["storageArray"]);		
		if (storageArray.length > 0) {
			for (var ii=0; ii<window.localStorage.storageArray.length; ii++) {
				
				// check parse posts even or odd, if odd or even				
				if (lengthForBackgroundColor === 1 || lengthForBackgroundColor === 3) {
					
					// even number? every other change background color
					if ( ii % 2 ) {							
						document.getElementById("localItems").innerHTML += "<div style='padding:1em; width:100%;'><h1>Question:</h1> " + storageArray[ii].question + "<br />" + "<h1>Answer:</h1>" + storageArray[ii].answer + "</div>";															
					} else { 								
						document.getElementById("localItems").innerHTML += "<div style='padding:1em; width:100%; background-color:#9999ff;'><h1>Question:</h1> " + storageArray[ii].question + "<br />" + "<h1>Answer:</h1>" + storageArray[ii].answer + "</div>";														
					}					
					
				} else {
					
					// even number? every other change background color
					if ( ii % 2 ) {							
						document.getElementById("localItems").innerHTML += "<div style='padding:1em; width:100%; background-color:#9999ff;'><h1>Question:</h1> " + storageArray[ii].question + "<br />" + "<h1>Answer:</h1>" + storageArray[ii].answer + "</div>";														
					} else { 																				
						document.getElementById("localItems").innerHTML += "<div style='padding:1em; width:100%;'><h1>Question:</h1> " + storageArray[ii].question + "<br />" + "<h1>Answer:</h1>" + storageArray[ii].answer + "</div>";	
					}																				
				}

				if(storageArray[ii].newAnswer){
					//document.getElementById("localItems").innerHTML += "<div style='padding:1em;'><button id='reportA' ng-click='reportA()'>Report Answer</button></div>"
					document.getElementById("reportA").style.display = 'block';
					storageArray[ii].newAnswer = false;
				}else{
					//document.getElementById("reportA").style.display = 'none';
					/*var string = document.getElementById("localItems").innerHTML;
					string = string.replace("<div style='padding:1em;'><button id='reportA' ng-click='reportA()'>Report Answer</button></div>", "");
					document.getElementById("localItems").innerHTML = string;
				}															
			}
		} else {
			// alert("DONT exists" + ii);	
		}		  
 	

  },
  error: function(error) {
    alert("Error: " + error.code + " " + error.message);
  }
  	
  });*/
	/*
 	$scope.reportA = function(){
 	console.log("ReportA");
 	navigator.notification.prompt(
        'Reason for report:',
        onPrompt,
        'Report',   
        ['Ok','Exit']
      );

      function onPrompt(results) {
        if(results.input1){
          var user = Parse.Object.extend("users");
          var query = new Parse.Query(user);

          query.equalTo("UserID", qResult.get("personID"));
          query.first().then(function(result) {
            result.add("report", qResult);
            result.save(null, {
              success: function(point) {
                    
              },
              error: function(point, error) {
                console.log("Failed to save. Syntax error. Not DB error.");
              }
            });

          });
        }else {
          qResult.set("status", 0);
          qResult.save(null, {
            success: function(point) {
                
            },
            error: function(point, error) {
                console.log("Failed to save. Syntax error. Not DB error.");
            }
          });
        }
        
        $scope.$apply(function() {
          $location.path("/startpage");
        });
      }
    }
 }*/
	
}]);